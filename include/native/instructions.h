#ifndef NATIVE_INSTRUCTIONS_H
#define NATIVE_INSTRUCTIONS_H

#include <stdint.h>

typedef void (*Instruction)(uint8_t* bytes);

// Execute instruction
void instruction_exec(uint8_t* bytes);
uint8_t instruction_width(uint8_t byte);

// Register instructions
void op_frame_new(uint8_t* bytes);
void op_frame_del(uint8_t* bytes);

void op_local_alloc(uint8_t* bytes);
void op_local_dealloc(uint8_t* bytes);
void op_local_realloc(uint8_t* bytes);

void op_arg_alloc(uint8_t* bytes);
void op_arg_dealloc(uint8_t* bytes);
void op_arg_realloc(uint8_t* bytes);

void op_arg_push_const(uint8_t* bytes);
void op_arg_push_local(uint8_t* bytes);
void op_arg_pull(uint8_t* bytes);

void op_return_push_const(uint8_t* bytes);
void op_return_push_local(uint8_t* bytes);
void op_return_pull(uint8_t* bytes);

void op_const_pull(uint8_t* bytes);

// Int8 instructions
void op_int8_new(uint8_t* bytes);
void op_int8_new_empty(uint8_t* bytes);

// Int16 instructions
void op_int16_new(uint8_t* bytes);
void op_int16_new_empty(uint8_t* bytes);

// Int32 instructions
void op_int32_new(uint8_t* bytes);
void op_int32_new_empty(uint8_t* bytes);

// Int64 instructions
void op_int64_new(uint8_t* bytes);
void op_int64_new_empty(uint8_t* bytes);

// uInt8 instructions
void op_uint8_new(uint8_t* bytes);
void op_uint8_new_empty(uint8_t* bytes);

// uInt16 instructions
void op_uint16_new(uint8_t* bytes);
void op_uint16_new_empty(uint8_t* bytes);

// uInt32 instructions
void op_uint32_new(uint8_t* bytes);
void op_uint32_new_empty(uint8_t* bytes);

// uInt64 instructions
void op_uint64_new(uint8_t* bytes);
void op_uint64_new_empty(uint8_t* bytes);

// Arr instructions
void op_arr_dealloc(uint8_t* bytes);
void op_arr_replace(uint8_t* bytes);
void op_arr_replace_at(uint8_t* bytes);
void op_arr_resize(uint8_t* bytes);
void op_arr_copy(uint8_t* bytes);
void op_arr_new_empty(uint8_t* bytes);

void op_arr_push_const(uint8_t* bytes);
void op_arr_push_local(uint8_t* bytes);
void op_arr_pull(uint8_t* bytes);

// Bool instructions
void op_bool_new(uint8_t* bytes);
void op_bool_new_empty(uint8_t* bytes);

// Char instructions
void op_char_dealloc(uint8_t* bytes);
void op_char_replace(uint8_t* bytes);
void op_char_copy(uint8_t* bytes);
void op_char_new_empty(uint8_t* bytes);

// Str instructions
void op_str_dealloc(uint8_t* bytes);
void op_str_replace(uint8_t* bytes);
void op_str_replace_at(uint8_t* bytes);
void op_str_resize(uint8_t* bytes);
void op_str_copy(uint8_t* bytes);
void op_str_new_empty(uint8_t* bytes);

void op_str_push_const(uint8_t* bytes);
void op_str_push_local(uint8_t* bytes);
void op_str_pull(uint8_t* bytes);

// Call instructions
void op_native_call(uint8_t* bytes);
void op_static_call(uint8_t* bytes);

// Redirect instructions
void op_goto(uint8_t* bytes);
void op_goto_if(uint8_t* bytes);
void op_goto_if_not(uint8_t* bytes);

#endif
