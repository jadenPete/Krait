#ifndef NATIVE_FUNCTIONS_H
#define NATIVE_FUNCTIONS_H

#include <stdint.h>

typedef void (*Native_Func)(void);
typedef uint8_t Native_Func_Width;

void print_bool(void);
void print_char(void);
void print_str(void);

void print_int8(void);
void print_int16(void);
void print_int32(void);
void print_int64(void);

void print_uint8(void);
void print_uint16(void);
void print_uint32(void);
void print_uint64(void);

void println(void);

#endif
