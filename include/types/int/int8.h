#ifndef TYPES_INT8
#define TYPES_INT8

#include <stdint.h>

typedef int8_t Type_Int8;

// Arithmetic operators
void type_int8_add(void);
void type_int8_subtract(void);

void type_int8_plus(void);
void type_int8_minus(void);

void type_int8_multiply(void);
void type_int8_divide(void);
void type_int8_modulo(void);

// Comparison operators
void type_int8_equal(void);
void type_int8_not_equal(void);

void type_int8_greater(void);
void type_int8_less(void);

void type_int8_greater_equal(void);
void type_int8_less_equal(void);

// Bitwise operators
void type_int8_bitwise_not(void);
void type_int8_bitwise_and(void);
void type_int8_bitwise_or(void);
void type_int8_bitwise_xor(void);

void type_int8_left_shift(void);
void type_int8_right_shift(void);

#endif
