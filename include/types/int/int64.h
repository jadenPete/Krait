#ifndef TYPES_INT64
#define TYPES_INT64

#include <stdint.h>

typedef int64_t Type_Int64;

// Arithmetic operators
void type_int64_add(void);
void type_int64_subtract(void);

void type_int64_plus(void);
void type_int64_minus(void);

void type_int64_multiply(void);
void type_int64_divide(void);
void type_int64_modulo(void);

// Comparison operators
void type_int64_equal(void);
void type_int64_not_equal(void);

void type_int64_greater(void);
void type_int64_less(void);

void type_int64_greater_equal(void);
void type_int64_less_equal(void);

// Bitwise operators
void type_int64_bitwise_not(void);
void type_int64_bitwise_and(void);
void type_int64_bitwise_or(void);
void type_int64_bitwise_xor(void);

void type_int64_left_shift(void);
void type_int64_right_shift(void);

#endif
