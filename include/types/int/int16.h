#ifndef TYPES_INT16
#define TYPES_INT16

#include <stdint.h>

typedef int16_t Type_Int16;

// Arithmetic operators
void type_int16_add(void);
void type_int16_subtract(void);

void type_int16_plus(void);
void type_int16_minus(void);

void type_int16_multiply(void);
void type_int16_divide(void);
void type_int16_modulo(void);

// Comparison operators
void type_int16_equal(void);
void type_int16_not_equal(void);

void type_int16_greater(void);
void type_int16_less(void);

void type_int16_greater_equal(void);
void type_int16_less_equal(void);

// Bitwise operators
void type_int16_bitwise_not(void);
void type_int16_bitwise_and(void);
void type_int16_bitwise_or(void);
void type_int16_bitwise_xor(void);

void type_int16_left_shift(void);
void type_int16_right_shift(void);

#endif
