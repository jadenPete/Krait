#ifndef TYPES_INT32
#define TYPES_INT32

#include <stdint.h>

typedef int32_t Type_Int32;

// Arithmetic operators
void type_int32_add(void);
void type_int32_subtract(void);

void type_int32_plus(void);
void type_int32_minus(void);

void type_int32_multiply(void);
void type_int32_divide(void);
void type_int32_modulo(void);

// Comparison operators
void type_int32_equal(void);
void type_int32_not_equal(void);

void type_int32_greater(void);
void type_int32_less(void);

void type_int32_greater_equal(void);
void type_int32_less_equal(void);

// Bitwise operators
void type_int32_bitwise_not(void);
void type_int32_bitwise_and(void);
void type_int32_bitwise_or(void);
void type_int32_bitwise_xor(void);

void type_int32_left_shift(void);
void type_int32_right_shift(void);

#endif
