#ifndef TYPES_ARR
#define TYPES_ARR

#include <stdint.h>

typedef uint32_t Type_Arr_Address;
typedef struct type_arr Type_Arr;

#include <storage/data.h>

struct type_arr {
	Variable* vars;
	Variable_ID type;
	Type_Arr_Address width;
};

// Builtin functions
void type_arr_width(void);

// Element accessor
Variable* type_arr_get(Type_Arr arr_, Type_Arr_Address addr);

// Modifies
void type_arr_dealloc(Type_Arr arr_);
void type_arr_replace(Type_Arr* dest, Type_Arr src);
void type_arr_replace_at(Type_Arr* dest, Type_Arr src, Type_Arr_Address offset);
void type_arr_resize(Type_Arr* arr_, Type_Arr_Address width);

// Creates
Type_Arr type_arr_copy(Type_Arr arr_);
Type_Arr type_arr_new(Variable* vars, Variable_ID type, Type_Arr_Address width);
Type_Arr type_arr_new_null(Variable_ID type, Type_Arr_Address width);
Type_Arr type_arr_new_empty(Variable_ID type, Type_Arr_Address width);

#endif
