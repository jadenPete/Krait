#ifndef TYPES_BOOL_H
#define TYPES_BOOL_H

#include <stdint.h>

typedef uint8_t Type_Bool;

// Logical Operators
void type_bool_not(void);
void type_bool_and(void);
void type_bool_or(void);

// Comparison operators
void type_bool_equal(void);
void type_bool_not_equal(void);

#endif
