#ifndef TYPES_UINT16
#define TYPES_UINT16

#include <stdint.h>

typedef uint16_t Type_uInt16;

// Arithmetic operators
void type_uint16_add(void);
void type_uint16_subtract(void);

void type_uint16_multiply(void);
void type_uint16_divide(void);
void type_uint16_modulo(void);

// Comparison operators
void type_uint16_equal(void);
void type_uint16_not_equal(void);

void type_uint16_greater(void);
void type_uint16_less(void);

void type_uint16_greater_equal(void);
void type_uint16_less_equal(void);

// Bitwise operators
void type_uint16_bitwise_not(void);
void type_uint16_bitwise_and(void);
void type_uint16_bitwise_or(void);
void type_uint16_bitwise_xor(void);

void type_uint16_left_shift(void);
void type_uint16_right_shift(void);

#endif
