#ifndef TYPES_UINT64
#define TYPES_UINT64

#include <stdint.h>

typedef uint64_t Type_uInt64;

// Arithmetic operators
void type_uint64_add(void);
void type_uint64_subtract(void);

void type_uint64_multiply(void);
void type_uint64_divide(void);
void type_uint64_modulo(void);

// Comparison operators
void type_uint64_equal(void);
void type_uint64_not_equal(void);

void type_uint64_greater(void);
void type_uint64_less(void);

void type_uint64_greater_equal(void);
void type_uint64_less_equal(void);

// Bitwise operators
void type_uint64_bitwise_not(void);
void type_uint64_bitwise_and(void);
void type_uint64_bitwise_or(void);
void type_uint64_bitwise_xor(void);

void type_uint64_left_shift(void);
void type_uint64_right_shift(void);

#endif
