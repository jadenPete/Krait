#ifndef TYPES_UINT32
#define TYPES_UINT32

#include <stdint.h>

typedef uint32_t Type_uInt32;

// Arithmetic operators
void type_uint32_add(void);
void type_uint32_subtract(void);

void type_uint32_multiply(void);
void type_uint32_divide(void);
void type_uint32_modulo(void);

// Comparison operators
void type_uint32_equal(void);
void type_uint32_not_equal(void);

void type_uint32_greater(void);
void type_uint32_less(void);

void type_uint32_greater_equal(void);
void type_uint32_less_equal(void);

// Bitwise operators
void type_uint32_bitwise_not(void);
void type_uint32_bitwise_and(void);
void type_uint32_bitwise_or(void);
void type_uint32_bitwise_xor(void);

void type_uint32_left_shift(void);
void type_uint32_right_shift(void);

#endif
