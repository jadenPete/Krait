#ifndef TYPES_UINT8
#define TYPES_UINT8

#include <stdint.h>

typedef uint8_t Type_uInt8;

// Arithmetic operators
void type_uint8_add(void);
void type_uint8_subtract(void);

void type_uint8_multiply(void);
void type_uint8_divide(void);
void type_uint8_modulo(void);

// Comparison operators
void type_uint8_equal(void);
void type_uint8_not_equal(void);

void type_uint8_greater(void);
void type_uint8_less(void);

void type_uint8_greater_equal(void);
void type_uint8_less_equal(void);

// Bitwise operators
void type_uint8_bitwise_not(void);
void type_uint8_bitwise_and(void);
void type_uint8_bitwise_or(void);
void type_uint8_bitwise_xor(void);

void type_uint8_left_shift(void);
void type_uint8_right_shift(void);

#endif
