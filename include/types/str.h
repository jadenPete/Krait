#ifndef TYPES_STR_H
#define TYPES_STR_H

#include <stdint.h>

typedef uint32_t Type_Str_Address;
typedef struct type_str Type_Str;

#include <types/char.h>

struct type_str {
	Type_Char* chars;
	Type_Str_Address width;
};

// Comparison operators
void type_str_equal(void);
void type_str_not_equal(void);

// Builtin functions
void type_str_width(void);

// Character accessor
Type_Char* type_str_get(Type_Str str_, Type_Str_Address addr);

// Modifies
void type_str_dealloc(Type_Str str_);
void type_str_replace(Type_Str* dest, Type_Str src);
void type_str_replace_at(Type_Str* dest, Type_Str src, Type_Str_Address offset);
void type_str_resize(Type_Str* str_, Type_Str_Address width);

// Creates
Type_Str type_str_copy(Type_Str str_);
Type_Str type_str_new(uint8_t* bytes, uint8_t* width);
Type_Str type_str_new_null(Type_Str_Address width);
Type_Str type_str_new_empty(void);

#endif
