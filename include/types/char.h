#ifndef TYPES_CHAR_H
#define TYPES_CHAR_H

#include <stdint.h>

typedef struct type_char {
	uint8_t* bytes;
	uint8_t width;
} Type_Char;

// Operators
void type_char_equal(void);
void type_char_not_equal(void);

// Builtin functions
void type_char_width(void);
void type_char_get_unicode(void);

// Modifies
void type_char_dealloc(Type_Char char_);
void type_char_replace(Type_Char* dest, Type_Char src);

// Creates
Type_Char type_char_copy(Type_Char char_);
Type_Char type_char_new(uint8_t* bytes, uint8_t* width);
Type_Char type_char_new_null(uint8_t width);
Type_Char type_char_new_empty(void);

#endif
