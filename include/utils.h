#ifndef UTILS_H
#define UTILS_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include <jansson.h>

typedef enum fatal_error {
	LOADER, PARSER, RUNTIME
} Fatal_Error;

void fatal_error(Fatal_Error type, const char* format, ...);

int16_t int16_new(uint8_t* bytes);
int32_t int32_new(uint8_t* bytes);
int64_t int64_new(uint8_t* bytes);

uint16_t uint16_new(uint8_t* bytes);
uint32_t uint32_new(uint8_t* bytes);
uint64_t uint64_new(uint8_t* bytes);

char* str_new(const char* str);
bool strntou16(const char* str, size_t width, uint16_t* dest);

#endif
