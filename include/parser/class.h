#ifndef PARSER_CLASS_H
#define PARSER_CLASS_H

#include <stdint.h>

#include <storage/data.h>
#include <storage/exec.h>

uint8_t* parse_function(Function* func_, uint8_t* bytes);
uint8_t* parse_function_pool(Class* class_, uint8_t* bytes);

uint8_t* parse_var(Variable* var, Variable_ID* type, uint8_t* bytes);
uint8_t* parse_const_pool(Exec_Address class_, uint8_t* bytes);

void parse_class(Exec_Address class_, uint8_t* bytes);

#endif
