#ifndef PARSER_JSON_CLASSES_H
#define PARSER_JSON_CLASSES_H

#include <jansson.h>

void parse_cj_main_class(void);
void parse_cj_main_function(void);

void free_classes_json(void);
void parse_classes_json(void);

#endif
