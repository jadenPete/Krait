#ifndef STORAGE_EXEC_H
#define STORAGE_EXEC_H

#include <stdint.h>

typedef uint8_t* Bytecode;
typedef struct function Function;
typedef struct class Class;
typedef struct exec_reg Exec_Reg;

typedef uint16_t Function_Address;
typedef uint16_t Class_Address;
typedef uint16_t Exec_Address;

struct function {
	Bytecode* bytecodes;
	Function_Address width;
};

struct class {
	Function* functions;
	Class_Address width;
};

struct exec_reg {
	Class* classes;
	Exec_Address width;
};

// Function registers
void function_alloc(Function* func_, Function_Address width);
void function_dealloc(Function func_);

// Class registers
void class_alloc(Class* class_, Class_Address width);
void class_dealloc(Class class_);

// Execution register
void exec_alloc(Exec_Address width);
void exec_dealloc(void);

// Register variable accessors
Bytecode* function_get(Function func_, Function_Address addr);
Function* class_get(Class class_, Class_Address addr);
Class* exec_get(Exec_Address addr);

// Register width accessors
Function_Address function_width(Function func_);
Class_Address class_width(Class class_);
Exec_Address exec_width(void);

// Main class / function accessors
Exec_Address* exec_get_main_class(void);
Class_Address* exec_get_main_function(void);

#endif
