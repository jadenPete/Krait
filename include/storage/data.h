#ifndef STORAGE_DATA_H
#define STORAGE_DATA_H

#include <stdint.h>

typedef union variable_ Variable;
typedef struct frame_ Frame;
typedef struct const_class Const_Class;
typedef struct const_reg Const_Reg;

// 0  = bool
// 1  = char
// 2  = str

// 3  = int8
// 4  = int16
// 5  = int32
// 6  = int64

// 7  = uint8
// 8  = uint16
// 9  = uint32
// 10 = uint64
typedef uint8_t Variable_ID;

typedef uint8_t Arg_Address;
typedef uint16_t Local_Address;
typedef uint16_t Const_Address;
typedef uint16_t Frame_Address;

#include <storage/exec.h>
#include <types/int/int8.h>
#include <types/int/int16.h>
#include <types/int/int32.h>
#include <types/int/int64.h>
#include <types/uint/uint8.h>
#include <types/uint/uint16.h>
#include <types/uint/uint32.h>
#include <types/uint/uint64.h>
#include <types/arr.h>
#include <types/bool.h>
#include <types/char.h>
#include <types/str.h>

union variable_ {
	Type_Bool bool_;
	Type_Char char_;
	Type_Str str_;

	Type_Int8 int8_;
	Type_Int16 int16_;
	Type_Int32 int32_;
	Type_Int64 int64_;

	Type_uInt8 uint8_;
	Type_uInt16 uint16_;
	Type_uInt32 uint32_;
	Type_uInt64 uint64_;

	Type_Arr arr_;
};

struct frame_ {
	Variable* local_reg;
	Variable* arg_reg;
	Variable return_reg;
};

struct const_class {
	Variable* vars;
	Variable_ID* ids;
	Const_Address width;
};

struct const_reg {
	Const_Class* classes;
	Exec_Address width;
};

// Frames
Frame* frames_get_current(void);
Frame* frames_get_next(void);

void frames_alloc(void);
void frames_dealloc(void);

void frames_increment(void);
void frames_decrement(void);

void frames_next(void);
void frames_previous(void);

// Local register
void local_alloc(Local_Address width);
void local_realloc(Local_Address width);
void local_dealloc(void);

// Argument register
void arg_alloc(Arg_Address width);
void arg_realloc(Arg_Address width);
void arg_dealloc(void);

// Constant register
void const_alloc_class(Exec_Address class_, Const_Address width);
void const_dealloc_class(Exec_Address class_);

void const_alloc(Exec_Address width);
void const_dealloc(void);

// Frame register accessors
Variable* local_get(Local_Address addr);
Variable* arg_get(Arg_Address addr);
Variable* return_get(void);

// Constant register accessors
void const_set_var(Exec_Address class_, Const_Address addr, Variable var);
void const_set_id(Exec_Address class_, Const_Address addr, Variable_ID id);

Variable const_get_var(Const_Address addr);
Variable_ID const_get_id(Const_Address addr);

Const_Address const_width_class(Exec_Address class_);
Exec_Address const_width(void);

// Utilities
void var_dealloc(Variable var, Variable_ID id);
Variable var_copy(Variable var, Variable_ID id);
Variable var_new_empty(Variable_ID id);

#endif
