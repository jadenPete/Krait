#ifndef LOADER_ARCHIVE_H
#define LOADER_ARCHIVE_H

#include <stdint.h>

#include <archive.h>
#include <archive_entry.h>

typedef uint32_t Files_Address;
typedef size_t File_Address;

typedef struct file File;
typedef struct required_file Required_File;

extern File* files;
extern Files_Address files_count;
extern Files_Address files_width;

extern Required_File classes;
extern Required_File manifest;

struct file {
	char* path;
	uint8_t* bytes;
	File_Address width;
};

struct required_file {
	char* bytes;
	File_Address width;
};

void fatal_archive_error(void);

void free_archive_files(void);
void free_archive(void);

void read_entry(void* bytes, size_t width);
void read_required_entry(struct archive_entry* entry, Required_File* file);

void load_entry(struct archive_entry* entry);
void load_archive(const char* path);

#endif
