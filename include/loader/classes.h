#ifndef LOADER_CLASSES_H
#define LOADER_CLASSES_H

#include <storage/exec.h>

void load_class(File file);
void load_classes(void);

#endif
