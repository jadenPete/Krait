#ifndef RUNTIME_H
#define RUNTIME_H

#include <storage/exec.h>

Exec_Address runtime_get_class(void);
Class_Address runtime_get_function(void);

void runtime_call(Exec_Address class_, Class_Address func_);
void runtime_redirect(Function_Address addr);

void runtime_exec(void);
void runtime_dealloc(void);
void runtime_start(void);

#endif
