#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include <archive.h>
#include <archive_entry.h>

#include <loader/archive.h>
#include <loader/classes.h>
#include <utils.h>

File* files = NULL;
Files_Address files_count = 0;
Files_Address files_width = 0;

Required_File classes = {.bytes = NULL};
Required_File manifest = {.bytes = NULL};

struct archive* archive = NULL;

void fatal_archive_error(void){
	fatal_error(LOADER, archive_error_string(archive));
}

void free_archive_files(void){
	if(files != NULL){
		for(Files_Address i = 0; i < files_count; i++){
			free(files[i].path);
			free(files[i].bytes);
		}

		free(files);
	}

	free(classes.bytes);
	free(manifest.bytes);
}

void free_archive(void){
	if(archive != NULL){
		if(archive_read_free(archive) != ARCHIVE_OK){
			fatal_archive_error();
		}

		archive = NULL;
	}
}

void read_entry(void* bytes, size_t width){
	while(true){
		la_ssize_t result = archive_read_data(archive, bytes, width);

		if(result < 0) fatal_archive_error();
		if(result == 0) break;
	}
}

void read_required_entry(struct archive_entry* entry, Required_File* file){
	file->width = archive_entry_size(entry);
	file->bytes = malloc(file->width * sizeof(char));

	read_entry(file->bytes, file->width);
}

void load_entry(struct archive_entry* entry){
	if(archive_entry_filetype(entry) == AE_IFREG){
		if(strcmp(archive_entry_pathname(entry), "manifest.json") == 0){
			read_required_entry(entry, &manifest);
		} else if(strcmp(archive_entry_pathname(entry), "classes.json") == 0){
			read_required_entry(entry, &classes);
		} else {
			if(++files_count > files_width){
				files_width += 10;
				files = realloc(files, files_width * sizeof(File));

				if(files == NULL){
					fatal_error(LOADER, "Failed to reallocate files buffer");
				}
			}

			files[files_count - 1].path = str_new(archive_entry_pathname(entry));
			files[files_count - 1].width = archive_entry_size(entry);
			files[files_count - 1].bytes = malloc(files[files_count - 1].width);

			read_entry(files[files_count - 1].bytes, files[files_count - 1].width);
		}
	}
}

void load_archive(const char* path){
	archive = archive_read_new();

	archive_read_support_filter_all(archive);
	archive_read_support_format_all(archive);

	if(archive_read_open_filename(archive, path, 10240) != ARCHIVE_OK){
		fatal_archive_error();
	}

	struct archive_entry* entry;

	while(true){
		int result = archive_read_next_header(archive, &entry);

		if(result == ARCHIVE_EOF) break;
		if(result < ARCHIVE_OK) fatal_archive_error();

		load_entry(entry);
	}

	free_archive();
}
