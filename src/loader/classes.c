#include <stdlib.h>
#include <string.h>

#include <loader/archive.h>
#include <loader/classes.h>
#include <parser/class.h>
#include <storage/exec.h>
#include <utils.h>

void load_class(File file){
	Exec_Address addr;
	bool success;

	if(strncmp(file.path, "./", 2) == 0){
		success = strntou16(file.path + 2, strlen(file.path + 2) - 3, &addr);
	} else {
		success = strntou16(file.path, strlen(file.path) - 3, &addr);
	}

    if(!success || addr >= files_count){
		fatal_error(LOADER, "Invalid filename: %s", file.path);
	}

	parse_class(addr, file.bytes);
}

void load_classes(void){
	if(files_count > 0){
		const_alloc(files_count);
		exec_alloc(files_count);
	}

	for(Files_Address i = 0; i < files_count; i++){
		load_class(files[i]);
	}
}
