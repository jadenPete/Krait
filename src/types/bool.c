#include <stdint.h>

#include <storage/data.h>
#include <types/bool.h>

void type_bool_not(void){
	return_get()->bool_ = !arg_get(0)->bool_;
}

void type_bool_and(void){
	return_get()->bool_ = arg_get(0)->bool_ && arg_get(1)->bool_;
}

void type_bool_or(void){
	return_get()->bool_ = arg_get(0)->bool_ || arg_get(1)->bool_;
}

void type_bool_equal(void){
	return_get()->bool_ = arg_get(0)->bool_ == arg_get(1)->bool_;
}

void type_bool_not_equal(void){
	return_get()->bool_ = arg_get(0)->bool_ != arg_get(1)->bool_;
}
