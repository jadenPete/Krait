#include <types/bool.h>
#include <types/uint/uint16.h>

#include <storage/data.h>

void type_uint16_add(void){
	return_get()->uint16_ = arg_get(0)->uint16_ + arg_get(1)->uint16_;
}

void type_uint16_subtract(void){
	return_get()->uint16_ = arg_get(0)->uint16_ - arg_get(1)->uint16_;
}

void type_uint16_multiply(void){
	return_get()->uint16_ = arg_get(0)->uint16_ * arg_get(1)->uint16_;
}

void type_uint16_divide(void){
	return_get()->uint16_ = arg_get(0)->uint16_ / arg_get(1)->uint16_;
}

void type_uint16_modulo(void){
	return_get()->uint16_ = arg_get(0)->uint16_ % arg_get(1)->uint16_;
}

void type_uint16_equal(void){
	return_get()->bool_ = arg_get(0)->uint16_ == arg_get(1)->uint16_;
}

void type_uint16_not_equal(void){
	return_get()->bool_ = arg_get(0)->uint16_ != arg_get(1)->uint16_;
}

void type_uint16_greater(void){
	return_get()->bool_ = arg_get(0)->uint16_ > arg_get(1)->uint16_;
}

void type_uint16_less(void){
	return_get()->bool_ = arg_get(0)->uint16_ < arg_get(1)->uint16_;
}

void type_uint16_greater_equal(void){
	return_get()->bool_ = arg_get(0)->uint16_ >= arg_get(1)->uint16_;
}

void type_uint16_less_equal(void){
	return_get()->bool_ = arg_get(0)->uint16_ <= arg_get(1)->uint16_;
}

void type_uint16_bitwise_not(void){
	return_get()->uint16_ = ~arg_get(0)->uint16_;
}

void type_uint16_bitwise_and(void){
	return_get()->uint16_ = arg_get(0)->uint16_ & arg_get(1)->uint16_;
}

void type_uint16_bitwise_or(void){
	return_get()->uint16_ = arg_get(0)->uint16_ | arg_get(1)->uint16_;
}

void type_uint16_bitwise_xor(void){
	return_get()->uint16_ = arg_get(0)->uint16_ ^ arg_get(1)->uint16_;
}

void type_uint16_left_shift(void){
	return_get()->uint16_ = arg_get(0)->uint16_ << arg_get(1)->uint8_;
}

void type_uint16_right_shift(void){
	return_get()->uint16_ = arg_get(0)->uint16_ >> arg_get(1)->uint8_;
}
