#include <types/bool.h>
#include <types/uint/uint32.h>

#include <storage/data.h>

void type_uint32_add(void){
	return_get()->uint32_ = arg_get(0)->uint32_ + arg_get(1)->uint32_;
}

void type_uint32_subtract(void){
	return_get()->uint32_ = arg_get(0)->uint32_ - arg_get(1)->uint32_;
}

void type_uint32_multiply(void){
	return_get()->uint32_ = arg_get(0)->uint32_ * arg_get(1)->uint32_;
}

void type_uint32_divide(void){
	return_get()->uint32_ = arg_get(0)->uint32_ / arg_get(1)->uint32_;
}

void type_uint32_modulo(void){
	return_get()->uint32_ = arg_get(0)->uint32_ % arg_get(1)->uint32_;
}

void type_uint32_equal(void){
	return_get()->bool_ = arg_get(0)->uint32_ == arg_get(1)->uint32_;
}

void type_uint32_not_equal(void){
	return_get()->bool_ = arg_get(0)->uint32_ != arg_get(1)->uint32_;
}

void type_uint32_greater(void){
	return_get()->bool_ = arg_get(0)->uint32_ > arg_get(1)->uint32_;
}

void type_uint32_less(void){
	return_get()->bool_ = arg_get(0)->uint32_ < arg_get(1)->uint32_;
}

void type_uint32_greater_equal(void){
	return_get()->bool_ = arg_get(0)->uint32_ >= arg_get(1)->uint32_;
}

void type_uint32_less_equal(void){
	return_get()->bool_ = arg_get(0)->uint32_ <= arg_get(1)->uint32_;
}

void type_uint32_bitwise_not(void){
	return_get()->uint32_ = ~arg_get(0)->uint32_;
}

void type_uint32_bitwise_and(void){
	return_get()->uint32_ = arg_get(0)->uint32_ & arg_get(1)->uint32_;
}

void type_uint32_bitwise_or(void){
	return_get()->uint32_ = arg_get(0)->uint32_ | arg_get(1)->uint32_;
}

void type_uint32_bitwise_xor(void){
	return_get()->uint32_ = arg_get(0)->uint32_ ^ arg_get(1)->uint32_;
}

void type_uint32_left_shift(void){
	return_get()->uint32_ = arg_get(0)->uint32_ << arg_get(1)->uint8_;
}

void type_uint32_right_shift(void){
	return_get()->uint32_ = arg_get(0)->uint32_ >> arg_get(1)->uint8_;
}
