#include <types/bool.h>
#include <types/uint/uint64.h>

#include <storage/data.h>

void type_uint64_add(void){
	return_get()->uint64_ = arg_get(0)->uint64_ + arg_get(1)->uint64_;
}

void type_uint64_subtract(void){
	return_get()->uint64_ = arg_get(0)->uint64_ - arg_get(1)->uint64_;
}

void type_uint64_multiply(void){
	return_get()->uint64_ = arg_get(0)->uint64_ * arg_get(1)->uint64_;
}

void type_uint64_divide(void){
	return_get()->uint64_ = arg_get(0)->uint64_ / arg_get(1)->uint64_;
}

void type_uint64_modulo(void){
	return_get()->uint64_ = arg_get(0)->uint64_ % arg_get(1)->uint64_;
}

void type_uint64_equal(void){
	return_get()->bool_ = arg_get(0)->uint64_ == arg_get(1)->uint64_;
}

void type_uint64_not_equal(void){
	return_get()->bool_ = arg_get(0)->uint64_ != arg_get(1)->uint64_;
}

void type_uint64_greater(void){
	return_get()->bool_ = arg_get(0)->uint64_ > arg_get(1)->uint64_;
}

void type_uint64_less(void){
	return_get()->bool_ = arg_get(0)->uint64_ < arg_get(1)->uint64_;
}

void type_uint64_greater_equal(void){
	return_get()->bool_ = arg_get(0)->uint64_ >= arg_get(1)->uint64_;
}

void type_uint64_less_equal(void){
	return_get()->bool_ = arg_get(0)->uint64_ <= arg_get(1)->uint64_;
}

void type_uint64_bitwise_not(void){
	return_get()->uint64_ = ~arg_get(0)->uint64_;
}

void type_uint64_bitwise_and(void){
	return_get()->uint64_ = arg_get(0)->uint64_ & arg_get(1)->uint64_;
}

void type_uint64_bitwise_or(void){
	return_get()->uint64_ = arg_get(0)->uint64_ | arg_get(1)->uint64_;
}

void type_uint64_bitwise_xor(void){
	return_get()->uint64_ = arg_get(0)->uint64_ ^ arg_get(1)->uint64_;
}

void type_uint64_left_shift(void){
	return_get()->uint64_ = arg_get(0)->uint64_ << arg_get(1)->uint8_;
}

void type_uint64_right_shift(void){
	return_get()->uint64_ = arg_get(0)->uint64_ >> arg_get(1)->uint8_;
}
