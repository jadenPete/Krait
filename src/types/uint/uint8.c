#include <types/bool.h>
#include <types/uint/uint8.h>

#include <storage/data.h>

void type_uint8_add(void){
	return_get()->uint8_ = arg_get(0)->uint8_ + arg_get(1)->uint8_;
}

void type_uint8_subtract(void){
	return_get()->uint8_ = arg_get(0)->uint8_ - arg_get(1)->uint8_;
}

void type_uint8_multiply(void){
	return_get()->uint8_ = arg_get(0)->uint8_ * arg_get(1)->uint8_;
}

void type_uint8_divide(void){
	return_get()->uint8_ = arg_get(0)->uint8_ / arg_get(1)->uint8_;
}

void type_uint8_modulo(void){
	return_get()->uint8_ = arg_get(0)->uint8_ % arg_get(1)->uint8_;
}

void type_uint8_equal(void){
	return_get()->bool_ = arg_get(0)->uint8_ == arg_get(1)->uint8_;
}

void type_uint8_not_equal(void){
	return_get()->bool_ = arg_get(0)->uint8_ != arg_get(1)->uint8_;
}

void type_uint8_greater(void){
	return_get()->bool_ = arg_get(0)->uint8_ > arg_get(1)->uint8_;
}

void type_uint8_less(void){
	return_get()->bool_ = arg_get(0)->uint8_ < arg_get(1)->uint8_;
}

void type_uint8_greater_equal(void){
	return_get()->bool_ = arg_get(0)->uint8_ >= arg_get(1)->uint8_;
}

void type_uint8_less_equal(void){
	return_get()->bool_ = arg_get(0)->uint8_ <= arg_get(1)->uint8_;
}

void type_uint8_bitwise_not(void){
	return_get()->uint8_ = ~arg_get(0)->uint8_;
}

void type_uint8_bitwise_and(void){
	return_get()->uint8_ = arg_get(0)->uint8_ & arg_get(1)->uint8_;
}

void type_uint8_bitwise_or(void){
	return_get()->uint8_ = arg_get(0)->uint8_ | arg_get(1)->uint8_;
}

void type_uint8_bitwise_xor(void){
	return_get()->uint8_ = arg_get(0)->uint8_ ^ arg_get(1)->uint8_;
}

void type_uint8_left_shift(void){
	return_get()->uint8_ = arg_get(0)->uint8_ << arg_get(1)->uint8_;
}

void type_uint8_right_shift(void){
	return_get()->uint8_ = arg_get(0)->uint8_ >> arg_get(1)->uint8_;
}
