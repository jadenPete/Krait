#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <storage/data.h>
#include <types/char.h>
#include <types/str.h>
#include <utils.h>

void type_str_equal(void){
	Type_Str str1 = arg_get(0)->str_;
	Type_Str str2 = arg_get(1)->str_;
	Type_Bool* result = &return_get()->bool_;

	if((*result = (str1.width == str2.width))){
		frames_increment();
		arg_alloc(2);

		for(Type_Str_Address i = 0; i < str1.width; i++){
			arg_get(0)->char_ = str1.chars[i];
			arg_get(1)->char_ = str2.chars[i];

			type_char_equal();

			if(!return_get()->bool_){
				*result = 0; break;
			}
		}

		arg_dealloc();
		frames_decrement();
	}
}

void type_str_not_equal(void){
	type_str_equal();
	return_get()->bool_ = !return_get()->bool_;
}

void type_str_width(void){
	return_get()->uint32_ = arg_get(0)->str_.width;
}

Type_Char* type_str_get(Type_Str str_, Type_Str_Address addr){
	return str_.chars + addr;
}

void type_str_dealloc(Type_Str str_){
	for(Type_Str_Address i = 0; i < str_.width; i++){
		type_char_dealloc(str_.chars[i]);
	}

	free(str_.chars);
}

void type_str_replace(Type_Str* dest, Type_Str src){
	type_str_dealloc(*dest);
	*dest = type_str_copy(src);
}

void type_str_replace_at(Type_Str* dest, Type_Str src, Type_Str_Address offset){
	if(src.width + offset > dest->width){
		fatal_error(RUNTIME, "str_replace_at: Destination string too small");
	}

	for(Type_Str_Address i = 0; i < src.width; i++){
		type_char_dealloc(dest->chars[offset + i]);
		dest->chars[offset + i] = type_char_copy(src.chars[i]);
	}
}

void type_str_resize(Type_Str* str_, Type_Str_Address width){
	for(Type_Str_Address i = width - 1; i < str_->width; i++){
		type_char_dealloc(str_->chars[i]);
	}

	str_->chars = realloc(str_->chars, width * sizeof(Type_Char));

	for(Type_Str_Address i = str_->width - 1; i < width; i++){
		str_->chars[i] = (Type_Char) {.bytes = NULL, .width = 0};
	}

	str_->width = width;
}

Type_Str type_str_copy(Type_Str str_){
	Type_Str new = type_str_new_null(str_.width);

	for(Type_Str_Address i = 0; i < str_.width; i++){
		new.chars[i] = type_char_copy(str_.chars[i]);
	}

	return new;
}

Type_Str type_str_new(uint8_t* bytes, uint8_t* width){
	Type_Str new = type_str_new_null(uint32_new(bytes));
	Type_Str_Address i;
	uint8_t char_width;

	bytes += 4;
	width = 0;

	for(i = 0; i < new.width; i++){
		new.chars[i] = type_char_new(bytes, &char_width);
		width += char_width;
	}

	return new;
}

Type_Str type_str_new_null(Type_Str_Address width){
	Type_Str new;

	new.chars = malloc(width * sizeof(Type_Char));
	new.width = width;

	for(Type_Str_Address i = 0; i < width; i++){
		new.chars[i] = (Type_Char) {.bytes = NULL, .width = 0};
	}

	return new;
}

Type_Str type_str_new_empty(void){
	return (Type_Str) {.chars = NULL, .width = 0};
}
