#include <stdlib.h>

#include <storage/data.h>
#include <types/arr.h>
#include <utils.h>

void type_arr_width(void){
	return_get()->uint32_ = arg_get(0)->arr_.width;
}

Variable* type_arr_get(Type_Arr arr_, Type_Arr_Address addr){
	return arr_.vars + addr;
}

void type_arr_dealloc(Type_Arr arr_){
	for(Type_Arr_Address i = 0; i < arr_.width; i++){
		var_dealloc(arr_.vars[i], arr_.type);
	}

	free(arr_.vars);
}

void type_arr_replace(Type_Arr* dest, Type_Arr src){
	type_arr_dealloc(*dest);
	*dest = type_arr_copy(src);
}

void type_arr_replace_at(Type_Arr* dest, Type_Arr src, Type_Arr_Address offset){
	if(src.width + offset > dest->width){
		fatal_error(RUNTIME, "arr_replace_at: Destination array too small");
	}

	for(Type_Arr_Address i = 0; i < src.width; i++){
		var_dealloc(dest->vars[offset + i], dest->type);
		dest->vars[offset + i] = var_copy(src.vars[i], src.type);
	}
}

void type_arr_resize(Type_Arr* arr_, Type_Arr_Address width){
	for(Type_Arr_Address i = width - 1; i < arr_->width; i++){
		var_dealloc(arr_->vars[i], arr_->type);
	}

	arr_->vars = realloc(arr_->vars, width * sizeof(Variable));

	for(Type_Arr_Address i = arr_->width - 1; i < width; i++){
		arr_->vars[i] = var_new_empty(arr_->type);
	}

	arr_->width = width;
}

Type_Arr type_arr_copy(Type_Arr arr_){
	Type_Arr new = type_arr_new_null(arr_.type, arr_.width);

	for(Type_Arr_Address i = 0; i < arr_.width; i++){
		new.vars[i] = var_copy(arr_.vars[i], arr_.type);
	}

	return new;
}

Type_Arr type_arr_new(Variable* vars, Variable_ID type, Type_Arr_Address width){
	Type_Arr new = type_arr_new_null(type, width);

	for(Type_Arr_Address i = 0; i < width; i++){
		new.vars[i] = vars[i];
	}

	return new;
}

Type_Arr type_arr_new_null(Variable_ID type, Type_Arr_Address width){
	Type_Arr new;

	new.vars = malloc(width * sizeof(Variable));
	new.type = type;
	new.width = width;

	for(Type_Arr_Address i = 0; i < width; i++){
		new.vars[i] = (Variable) {0};
	}

	return new;
}

Type_Arr type_arr_new_empty(Variable_ID type, Type_Arr_Address width){
	Type_Arr new;

	new.vars = malloc(width * sizeof(Variable));
	new.type = type;
	new.width = width;

	for(Type_Arr_Address i = 0; i < width; i++){
		new.vars[i] = var_new_empty(type);
	}

	return new;
}
