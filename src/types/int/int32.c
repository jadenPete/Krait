#include <types/bool.h>
#include <types/int/int32.h>

#include <storage/data.h>

void type_int32_add(void){
	return_get()->int32_ = arg_get(0)->int32_ + arg_get(1)->int32_;
}

void type_int32_subtract(void){
	return_get()->int32_ = arg_get(0)->int32_ - arg_get(1)->int32_;
}

void type_int32_plus(void){
	return_get()->int32_ = +arg_get(0)->int32_;
}

void type_int32_minus(void){
	return_get()->int32_ = -arg_get(0)->int32_;
}

void type_int32_multiply(void){
	return_get()->int32_ = arg_get(0)->int32_ * arg_get(1)->int32_;
}

void type_int32_divide(void){
	return_get()->int32_ = arg_get(0)->int32_ / arg_get(1)->int32_;
}

void type_int32_modulo(void){
	return_get()->int32_ = arg_get(0)->int32_ % arg_get(1)->int32_;
}

void type_int32_equal(void){
	return_get()->bool_ = arg_get(0)->int32_ == arg_get(1)->int32_;
}

void type_int32_not_equal(void){
	return_get()->bool_ = arg_get(0)->int32_ != arg_get(1)->int32_;
}

void type_int32_greater(void){
	return_get()->bool_ = arg_get(0)->int32_ > arg_get(1)->int32_;
}

void type_int32_less(void){
	return_get()->bool_ = arg_get(0)->int32_ < arg_get(1)->int32_;
}

void type_int32_greater_equal(void){
	return_get()->bool_ = arg_get(0)->int32_ >= arg_get(1)->int32_;
}

void type_int32_less_equal(void){
	return_get()->bool_ = arg_get(0)->int32_ <= arg_get(1)->int32_;
}

void type_int32_bitwise_not(void){
	return_get()->int32_ = ~arg_get(0)->int32_;
}

void type_int32_bitwise_and(void){
	return_get()->int32_ = arg_get(0)->int32_ & arg_get(1)->int32_;
}

void type_int32_bitwise_or(void){
	return_get()->int32_ = arg_get(0)->int32_ | arg_get(1)->int32_;
}

void type_int32_bitwise_xor(void){
	return_get()->int32_ = arg_get(0)->int32_ ^ arg_get(1)->int32_;
}

void type_int32_left_shift(void){
	return_get()->int32_ = arg_get(0)->int32_ << arg_get(1)->uint8_;
}

void type_int32_right_shift(void){
	return_get()->int32_ = arg_get(0)->int32_ >> arg_get(1)->uint8_;
}
