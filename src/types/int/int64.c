#include <types/bool.h>
#include <types/int/int64.h>

#include <storage/data.h>

void type_int64_add(void){
	return_get()->int64_ = arg_get(0)->int64_ + arg_get(1)->int64_;
}

void type_int64_subtract(void){
	return_get()->int64_ = arg_get(0)->int64_ - arg_get(1)->int64_;
}

void type_int64_plus(void){
	return_get()->int64_ = +arg_get(0)->int64_;
}

void type_int64_minus(void){
	return_get()->int64_ = -arg_get(0)->int64_;
}

void type_int64_multiply(void){
	return_get()->int64_ = arg_get(0)->int64_ * arg_get(1)->int64_;
}

void type_int64_divide(void){
	return_get()->int64_ = arg_get(0)->int64_ / arg_get(1)->int64_;
}

void type_int64_modulo(void){
	return_get()->int64_ = arg_get(0)->int64_ % arg_get(1)->int64_;
}

void type_int64_equal(void){
	return_get()->bool_ = arg_get(0)->int64_ == arg_get(1)->int64_;
}

void type_int64_not_equal(void){
	return_get()->bool_ = arg_get(0)->int64_ != arg_get(1)->int64_;
}

void type_int64_greater(void){
	return_get()->bool_ = arg_get(0)->int64_ > arg_get(1)->int64_;
}

void type_int64_less(void){
	return_get()->bool_ = arg_get(0)->int64_ < arg_get(1)->int64_;
}

void type_int64_greater_equal(void){
	return_get()->bool_ = arg_get(0)->int64_ >= arg_get(1)->int64_;
}

void type_int64_less_equal(void){
	return_get()->bool_ = arg_get(0)->int64_ <= arg_get(1)->int64_;
}

void type_int64_bitwise_not(void){
	return_get()->int64_ = ~arg_get(0)->int64_;
}

void type_int64_bitwise_and(void){
	return_get()->int64_ = arg_get(0)->int64_ & arg_get(1)->int64_;
}

void type_int64_bitwise_or(void){
	return_get()->int64_ = arg_get(0)->int64_ | arg_get(1)->int64_;
}

void type_int64_bitwise_xor(void){
	return_get()->int64_ = arg_get(0)->int64_ ^ arg_get(1)->int64_;
}

void type_int64_left_shift(void){
	return_get()->int64_ = arg_get(0)->int64_ << arg_get(1)->uint8_;
}

void type_int64_right_shift(void){
	return_get()->int64_ = arg_get(0)->int64_ >> arg_get(1)->uint8_;
}
