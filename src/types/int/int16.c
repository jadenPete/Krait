#include <types/bool.h>
#include <types/int/int16.h>

#include <storage/data.h>

void type_int16_add(void){
	return_get()->int16_ = arg_get(0)->int16_ + arg_get(1)->int16_;
}

void type_int16_subtract(void){
	return_get()->int16_ = arg_get(0)->int16_ - arg_get(1)->int16_;
}

void type_int16_plus(void){
	return_get()->int16_ = +arg_get(0)->int16_;
}

void type_int16_minus(void){
	return_get()->int16_ = -arg_get(0)->int16_;
}

void type_int16_multiply(void){
	return_get()->int16_ = arg_get(0)->int16_ * arg_get(1)->int16_;
}

void type_int16_divide(void){
	return_get()->int16_ = arg_get(0)->int16_ / arg_get(1)->int16_;
}

void type_int16_modulo(void){
	return_get()->int16_ = arg_get(0)->int16_ % arg_get(1)->int16_;
}

void type_int16_equal(void){
	return_get()->bool_ = arg_get(0)->int16_ == arg_get(1)->int16_;
}

void type_int16_not_equal(void){
	return_get()->bool_ = arg_get(0)->int16_ != arg_get(1)->int16_;
}

void type_int16_greater(void){
	return_get()->bool_ = arg_get(0)->int16_ > arg_get(1)->int16_;
}

void type_int16_less(void){
	return_get()->bool_ = arg_get(0)->int16_ < arg_get(1)->int16_;
}

void type_int16_greater_equal(void){
	return_get()->bool_ = arg_get(0)->int16_ >= arg_get(1)->int16_;
}

void type_int16_less_equal(void){
	return_get()->bool_ = arg_get(0)->int16_ <= arg_get(1)->int16_;
}

void type_int16_bitwise_not(void){
	return_get()->int16_ = ~arg_get(0)->int16_;
}

void type_int16_bitwise_and(void){
	return_get()->int16_ = arg_get(0)->int16_ & arg_get(1)->int16_;
}

void type_int16_bitwise_or(void){
	return_get()->int16_ = arg_get(0)->int16_ | arg_get(1)->int16_;
}

void type_int16_bitwise_xor(void){
	return_get()->int16_ = arg_get(0)->int16_ ^ arg_get(1)->int16_;
}

void type_int16_left_shift(void){
	return_get()->int16_ = arg_get(0)->int16_ << arg_get(1)->uint8_;
}

void type_int16_right_shift(void){
	return_get()->int16_ = arg_get(0)->int16_ >> arg_get(1)->uint8_;
}
