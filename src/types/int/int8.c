#include <types/bool.h>
#include <types/int/int8.h>

#include <storage/data.h>

void type_int8_add(void){
	return_get()->int8_ = arg_get(0)->int8_ + arg_get(1)->int8_;
}

void type_int8_subtract(void){
	return_get()->int8_ = arg_get(0)->int8_ - arg_get(1)->int8_;
}

void type_int8_plus(void){
	return_get()->int8_ = +arg_get(0)->int8_;
}

void type_int8_minus(void){
	return_get()->int8_ = -arg_get(0)->int8_;
}

void type_int8_multiply(void){
	return_get()->int8_ = arg_get(0)->int8_ * arg_get(1)->int8_;
}

void type_int8_divide(void){
	return_get()->int8_ = arg_get(0)->int8_ / arg_get(1)->int8_;
}

void type_int8_modulo(void){
	return_get()->int8_ = arg_get(0)->int8_ % arg_get(1)->int8_;
}

void type_int8_equal(void){
	return_get()->bool_ = arg_get(0)->int8_ == arg_get(1)->int8_;
}

void type_int8_not_equal(void){
	return_get()->bool_ = arg_get(0)->int8_ != arg_get(1)->int8_;
}

void type_int8_greater(void){
	return_get()->bool_ = arg_get(0)->int8_ > arg_get(1)->int8_;
}

void type_int8_less(void){
	return_get()->bool_ = arg_get(0)->int8_ < arg_get(1)->int8_;
}

void type_int8_greater_equal(void){
	return_get()->bool_ = arg_get(0)->int8_ >= arg_get(1)->int8_;
}

void type_int8_less_equal(void){
	return_get()->bool_ = arg_get(0)->int8_ <= arg_get(1)->int8_;
}

void type_int8_bitwise_not(void){
	return_get()->int8_ = ~arg_get(0)->int8_;
}

void type_int8_bitwise_and(void){
	return_get()->int8_ = arg_get(0)->int8_ & arg_get(1)->int8_;
}

void type_int8_bitwise_or(void){
	return_get()->int8_ = arg_get(0)->int8_ | arg_get(1)->int8_;
}

void type_int8_bitwise_xor(void){
	return_get()->int8_ = arg_get(0)->int8_ ^ arg_get(1)->int8_;
}

void type_int8_left_shift(void){
	return_get()->int8_ = arg_get(0)->int8_ << arg_get(1)->uint8_;
}

void type_int8_right_shift(void){
	return_get()->int8_ = arg_get(0)->int8_ >> arg_get(1)->uint8_;
}
