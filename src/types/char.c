#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <storage/data.h>
#include <types/uint/uint32.h>
#include <types/bool.h>
#include <types/char.h>
#include <utils.h>

void type_char_equal(void){
	for(uint8_t i = 0; i < arg_get(0)->char_.width; i++){
		if(arg_get(0)->char_.bytes[i] != arg_get(1)->char_.bytes[i]){
			return_get()->bool_ = 0; return;
		}
	}

	return_get()->bool_ = 1;
}

void type_char_not_equal(void){
	type_char_equal();
	return_get()->bool_ = !return_get()->bool_;
}

void type_char_width(void){
	return_get()->uint8_ = arg_get(0)->char_.width;
}

void type_char_get_unicode(void){
	if(arg_get(0)->char_.width == 1){
		return_get()->uint32_ = ((Type_uInt32) arg_get(0)->char_.bytes[0]) << 24;
	} else if(arg_get(0)->char_.width == 2){
		return_get()->uint32_ = ((Type_uInt32) ((arg_get(0)->char_.bytes[1] & 0x3F) | (arg_get(0)->char_.bytes[0] << 6))) << 16;
		return_get()->uint32_ |= ((Type_uInt32) ((arg_get(0)->char_.bytes[0] >> 2) & 0x07)) << 24;
	} else if(arg_get(0)->char_.width == 3){
		return_get()->uint32_ = ((Type_uInt32) ((arg_get(0)->char_.bytes[1] >> 2) & 0x0F) | (arg_get(0)->char_.bytes[0] << 4)) << 24;
		return_get()->uint32_ |= ((Type_uInt32) ((arg_get(0)->char_.bytes[2] & 0x3F) | (arg_get(0)->char_.bytes[1] << 6))) << 16;
	} else {
		return_get()->uint32_ = ((Type_uInt32) (((arg_get(0)->char_.bytes[1] >> 4) & 0x03) | ((arg_get(0)->char_.bytes[0] << 2) & 0x1C))) << 24;
		return_get()->uint32_ |= ((Type_uInt32) (((arg_get(0)->char_.bytes[2] >> 2) & 0x0F) | (arg_get(0)->char_.bytes[1] << 4))) << 16;
		return_get()->uint32_ |= ((Type_uInt32) ((arg_get(0)->char_.bytes[3] & 0x3F) | (arg_get(0)->char_.bytes[2] << 6))) << 8;
	}
}

void type_char_dealloc(Type_Char char_){
	free(char_.bytes);
}

void type_char_replace(Type_Char* dest, Type_Char src){
	type_char_dealloc(*dest);
	*dest = type_char_copy(src);
}

Type_Char type_char_copy(Type_Char char_){
	Type_Char new = type_char_new_null(char_.width);

	memcpy(new.bytes, char_.bytes, char_.width);

	return new;
}

Type_Char type_char_new(uint8_t* bytes, uint8_t* width){
	Type_Char new;

	if(*bytes >> 7 == 0x00){
		new = type_char_new_null(1);
	} else if(*bytes >> 5 == 0x06){
		new = type_char_new_null(2);
	} else if(*bytes >> 4 == 0x0E){
		new = type_char_new_null(3);
	} else if(*bytes >> 3 == 0x1E){
		new = type_char_new_null(4);
	} else {
		fatal_error(RUNTIME, "char_new: Byte 1 invalid");
	}

	memcpy(new.bytes, bytes, new.width);
	*width = new.width;

	return new;
}

Type_Char type_char_new_null(uint8_t width){
	return (Type_Char) {.bytes = malloc(width), .width = width};
}

Type_Char type_char_new_empty(void){
	return (Type_Char) {.bytes = calloc(1, 1), .width = 1};
}
