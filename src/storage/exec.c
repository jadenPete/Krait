#include <stdlib.h>
#include <storage/exec.h>

// Execution register
Exec_Reg exec_reg = {.classes = NULL};

Exec_Address main_class;
Class_Address main_function;

void function_alloc(Function* func_, Function_Address width){
	func_->bytecodes = malloc(width * sizeof(Bytecode));
	func_->width = width;
}

void function_dealloc(Function func_){
	free(func_.bytecodes);
}

void class_alloc(Class* class_, Class_Address width){
	class_->functions = malloc(width * sizeof(Function));
	class_->width = width;

	for(Class_Address i = 0; i < width; i++){
		class_->functions[i].bytecodes = NULL;
	}
}

void class_dealloc(Class class_){
	if(class_.functions != NULL){
		for(Class_Address i = 0; i < class_.width; i++){
			function_dealloc(class_.functions[i]);
		}

		free(class_.functions);
	}
}

void exec_alloc(Exec_Address width){
	exec_reg.classes = malloc(width * sizeof(Class));
	exec_reg.width = width;

	for(Exec_Address i = 0; i < width; i++){
		exec_reg.classes[i].functions = NULL;
	}
}

void exec_dealloc(void){
	if(exec_reg.classes != NULL){
		for(Exec_Address i = 0; i < exec_reg.width; i++){
			class_dealloc(exec_reg.classes[i]);
		}

		free(exec_reg.classes);
	}
}

Bytecode* function_get(Function func_, Function_Address addr){
	return func_.bytecodes + addr;
}

Function* class_get(Class class_, Class_Address addr){
	return class_.functions + addr;
}

Class* exec_get(Exec_Address addr){
	return exec_reg.classes + addr;
}

Function_Address function_width(Function func_){
	return func_.width;
}

Class_Address class_width(Class class_){
	return class_.width;
}

Exec_Address exec_width(void){
	return exec_reg.width;
}

Exec_Address* exec_get_main_class(void){
	return &main_class;
}

Class_Address* exec_get_main_function(void){
	return &main_function;
}
