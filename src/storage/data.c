#include <stdint.h>
#include <stdlib.h>

#include <storage/data.h>
#include <types/arr.h>
#include <types/char.h>
#include <types/str.h>
#include <runtime.h>

// Frames
Frame_Address frames_width = 0;
Frame_Address frames_current;
Frame* frames = NULL;

// Constant register
Const_Reg const_reg = {.classes = NULL};

Frame* frames_get_current(void){
	return frames + frames_current;
}

Frame* frames_get_next(void){
	if(frames_width == frames_current + 2){
		return frames + frames_current + 1;
	} else {
		return frames + frames_current;
	}
}

void frames_alloc(void){
	frames_width = 1;
	frames_current = 0;

	frames = malloc(sizeof(Frame*));
}

void frames_dealloc(void){
	free(frames);
}

void frames_increment(void){
	frames = realloc(frames, sizeof(Frame) * ++frames_width);
}

void frames_decrement(void){
	frames = realloc(frames, sizeof(Frame) * --frames_width);
}

void frames_next(void){
	frames_current++;
}

void frames_previous(void){
	frames_current--;
}

void local_alloc(Local_Address width){
	frames_get_current()->local_reg = malloc(width * sizeof(Variable));
}

void local_realloc(Local_Address width){
	frames_get_current()->local_reg = realloc(frames_get_current()->local_reg, width * sizeof(Variable));
}

void local_dealloc(void){
	free(frames_get_current()->local_reg);
}

void arg_alloc(Arg_Address width){
	frames_get_next()->arg_reg = malloc(width * sizeof(Variable));
}

void arg_realloc(Arg_Address width){
	frames_get_next()->arg_reg = realloc(frames_get_next()->arg_reg, width * sizeof(Variable));
}

void arg_dealloc(void){
	free(frames_get_next()->arg_reg);
}

void const_alloc_class(Exec_Address class_, Const_Address width){
	if(width > 0){
		const_reg.classes[class_] = (Const_Class) {
			.vars = malloc(width * sizeof(Variable)),
			.ids = malloc(width * sizeof(Variable_ID)),
			.width = width
		};
	}
}

void const_dealloc_class(Exec_Address class_){
	if(const_reg.classes[class_].width > 0){
		for(Const_Address i = 0; i < const_reg.classes[class_].width; i++){
			var_dealloc(const_reg.classes[class_].vars[i], const_reg.classes[class_].ids[i]);
		}

		free(const_reg.classes[class_].vars);
		free(const_reg.classes[class_].ids);
	}
}

void const_alloc(Exec_Address width){
	const_reg.classes = malloc(width * sizeof(Const_Class));
	const_reg.width = width;

	for(Exec_Address i = 0; i < width; i++){
		const_reg.classes[i] = (Const_Class) {
			.vars = NULL,
			.ids = NULL,
			.width = 0
		};
	}
}

void const_dealloc(void){
	if(const_reg.classes != NULL){
		for(Exec_Address i = 0; i < const_reg.width; i++){
			const_dealloc_class(i);
		}

		free(const_reg.classes);
	}
}

Variable* local_get(Local_Address addr){
	return frames_get_current()->local_reg + addr;
}

Variable* arg_get(Arg_Address addr){
	return frames_get_next()->arg_reg + addr;
}

Variable* return_get(void){
	return &frames_get_next()->return_reg;
}

void const_set_var(Exec_Address class_, Const_Address addr, Variable var){
	const_reg.classes[class_].vars[addr] = var;
}

void const_set_id(Exec_Address class_, Const_Address addr, Variable_ID id){
	const_reg.classes[class_].ids[addr] = id;
}

Variable const_get_var(Const_Address addr){
	return const_reg.classes[runtime_get_class()].vars[addr];
}

Variable_ID const_get_id(Const_Address addr){
	return const_reg.classes[runtime_get_class()].ids[addr];
}

Const_Address const_width_class(Exec_Address class_){
	return const_reg.classes[class_].width;
}

Exec_Address const_width(void){
	return const_reg.width;
}

void var_dealloc(Variable var, Variable_ID id){
	switch(id){
		case 1: type_char_dealloc(var.char_); break;
		case 2: type_str_dealloc(var.str_); break;
		case 11: type_arr_dealloc(var.arr_);
	}
}

Variable var_copy(Variable var, Variable_ID id){
	switch(id){
		case 1: return (Variable) {.char_ = type_char_copy(var.char_)};
		case 2: return (Variable) {.str_ = type_str_copy(var.str_)};
	}

	return var;
}

Variable var_new_empty(Variable_ID id){
	switch(id){
		case 0:  return (Variable) {.bool_ = 0};
		case 1:  return (Variable) {.char_ = type_char_new_empty()};
		case 2:  return (Variable) {.str_ = type_str_new_empty()};

		case 3:  return (Variable) {.int8_ = 0};
		case 4:  return (Variable) {.int16_ = 0};
		case 5:  return (Variable) {.int32_ = 0};
		case 6:  return (Variable) {.int64_ = 0};

		case 7:  return (Variable) {.uint8_ = 0};
		case 8:  return (Variable) {.uint16_ = 0};
		case 9:  return (Variable) {.uint32_ = 0};
		case 10: return (Variable) {.uint8_ = 0};
	}

	return (Variable) {0};
}
