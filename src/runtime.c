#include <stdbool.h>
#include <stddef.h>

#include <native/instructions.h>
#include <storage/data.h>
#include <storage/exec.h>
#include <runtime.h>
#include <utils.h>

bool redirect_toggle = false;
Function_Address redirect_bytecode;

Exec_Address current_class;
Class_Address current_func;

Exec_Address runtime_get_class(void){
	return current_class;
}

Class_Address runtime_get_function(void){
	return current_func;
}

void runtime_call(Exec_Address class_, Class_Address func_){
	Exec_Address old_class = current_class;
	Exec_Address old_func = current_func;

	current_class = class_;
	current_func = func_;

	runtime_exec();

	current_class = old_class;
	current_func = old_func;
}

void runtime_redirect(Function_Address addr){
	redirect_toggle = true;
	redirect_bytecode = addr;
}

void runtime_exec(void){
	Class class_ = *exec_get(current_class);
	Function func_ = *class_get(class_, current_func);

	for(Function_Address i = 0; i < function_width(func_);){
		instruction_exec(*function_get(func_, i));

		if(redirect_toggle){
			redirect_toggle = false;
			i = redirect_bytecode;
		} else {
			i++;
		}
	}
}

void runtime_dealloc(void){
	const_dealloc();
	exec_dealloc();
	frames_dealloc();
}

void runtime_start(void){
	current_class = *exec_get_main_class();
	current_func = *exec_get_main_function();

	if(current_class >= exec_width()){
		fatal_error(RUNTIME, "Class %u does not exist", current_class);
	}

	if(current_func >= class_width(*exec_get(current_class))){
		fatal_error(RUNTIME, "Function %u of class %u does not exist", current_func, current_class);
	}

	frames_alloc();
	runtime_exec();
	runtime_dealloc();
}
