#include <parser/json/classes.h>

#include <loader/archive.h>
#include <loader/classes.h>
#include <storage/exec.h>
#include <main.h>
#include <runtime.h>
#include <utils.h>

int main(int argc, char* argv[]){
	if(argc == 2){
		load_archive(argv[1]);
		load_classes();
		parse_classes_json();
		runtime_start();
		free_archive_files();
	} else {
		fatal_error(LOADER, "Expected file path");
	}

	return 0;
}
