#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <jansson.h>

#include <loader/archive.h>
#include <parser/json/classes.h>
#include <runtime.h>
#include <utils.h>

void fatal_error(Fatal_Error type, const char* format, ...){
	va_list vl;
	va_start(vl, format);

	switch(type){
		case LOADER: fprintf(stderr, "Loading"); break;
		case PARSER: fprintf(stderr, "Parsing"); break;
		case RUNTIME: fprintf(stderr, "Runtime");
	}
	
	fprintf(stderr, " error: ");
	vfprintf(stderr, format, vl);
	fprintf(stderr, "\n");

	runtime_dealloc();
	free_classes_json();
	free_archive_files();
	free_archive();

	va_end(vl);
	exit(1);
}

int16_t int16_new(uint8_t* bytes){
	return (uint16_t) bytes[0] << 8 | bytes[1];
}

int32_t int32_new(uint8_t* bytes){
	return (uint32_t) bytes[0] << 24 |
	       (uint32_t) bytes[1] << 16 |
	       (uint32_t) bytes[2] << 8  |
	       (uint32_t) bytes[3];
}

int64_t int64_new(uint8_t* bytes){
	return (uint64_t) bytes[0] << 56 |
	       (uint64_t) bytes[1] << 48 |
	       (uint64_t) bytes[2] << 40 |
	       (uint64_t) bytes[3] << 32 |
		   (uint64_t) bytes[4] << 24 |
		   (uint64_t) bytes[5] << 16 |
		   (uint64_t) bytes[6] << 8  |
		   (uint64_t) bytes[7];
}

uint16_t uint16_new(uint8_t* bytes){
	return (uint16_t) bytes[0] << 8 | bytes[1];
}

uint32_t uint32_new(uint8_t* bytes){
	return (uint32_t) bytes[0] << 24 |
	       (uint32_t) bytes[1] << 16 |
	       (uint32_t) bytes[2] << 8  |
	       (uint32_t) bytes[3];
}

uint64_t uint64_new(uint8_t* bytes){
	return (uint64_t) bytes[0] << 56 |
	       (uint64_t) bytes[1] << 48 |
	       (uint64_t) bytes[2] << 40 |
	       (uint64_t) bytes[3] << 32 |
		   (uint64_t) bytes[4] << 24 |
		   (uint64_t) bytes[5] << 16 |
		   (uint64_t) bytes[6] << 8  |
		   (uint64_t) bytes[7];
}

char* str_new(const char* str){
	return strcpy(malloc(strlen(str) + 1), str);
}

bool strntou16(const char* str, size_t width, uint16_t* dest){
	*dest = 0;

	for(size_t i = 0; i < width; i++){
		if(str[i] < '0' || str[i] > '9'){
			return false;
		}

		*dest = *dest * 10 + str[i] - '0';
	}

	return true;
}
