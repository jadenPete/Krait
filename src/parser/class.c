#include <stdint.h>
#include <stdlib.h>

#include <native/instructions.h>
#include <parser/class.h>
#include <storage/data.h>
#include <storage/exec.h>
#include <types/arr.h>
#include <types/char.h>
#include <types/str.h>
#include <utils.h>

uint8_t* parse_function(Function* func_, uint8_t* bytes){
	function_alloc(func_, uint16_new(bytes)); bytes += 2;

	for(Function_Address i = 0; i < function_width(*func_); i++){
		*function_get(*func_, i) = bytes;
		bytes += instruction_width(*bytes) + 1;
	}

	return bytes;
}

uint8_t* parse_function_pool(Class* class_, uint8_t* bytes){
	class_alloc(class_, uint16_new(bytes)); bytes += 2;

	for(Class_Address i = 0; i < class_width(*class_); i++){
		bytes = parse_function(class_get(*class_, i), bytes);
	}

	return bytes;
}

uint8_t* parse_var(Variable* var, Variable_ID* type, uint8_t* bytes){
	uint8_t* width = NULL;

	if(type != NULL){
		*type = *bytes++;
	}

	switch(*type){
		case 0: var->bool_ = *bytes; return bytes + 1;
		case 1: var->char_ = type_char_new(bytes, width); break;
		case 2: var->str_ = type_str_new(bytes, width); break;

		case 3: var->int8_ = *bytes; return bytes + 1;
		case 4: var->int16_ = int16_new(bytes); return bytes + 2;
		case 5: var->int32_ = int32_new(bytes); return bytes + 4;
		case 6: var->int64_ = int64_new(bytes); return bytes + 8;

		case 7: var->uint8_ = *bytes; return bytes + 1;
		case 8: var->uint16_ = uint16_new(bytes); return bytes + 2;
		case 9: var->uint32_ = uint32_new(bytes); return bytes + 4;
		case 10: var->uint64_ = uint64_new(bytes); return bytes + 8;

		case 11:{
			Type_Arr_Address width = uint32_new(bytes);
			Variable* vars = malloc(width * sizeof(Variable));
			Variable_ID type = *(bytes += 4);

			for(Type_Arr_Address i = 0; i < width; i++){
				bytes = parse_var(vars + i, NULL, bytes);
			}

			var->arr_ = type_arr_new(vars, type, width);
		}
	}

	if(width == NULL){
		return bytes + *width;
	} else {
		return bytes;
	}
}

uint8_t* parse_const_pool(Exec_Address class_, uint8_t* bytes){
	const_alloc_class(class_, uint16_new(bytes)); bytes += 2;

	Variable var;
	Variable_ID type;

	for(Const_Address i = 0; i < const_width_class(class_); i++){
		bytes = parse_var(&var, &type, bytes);

		const_set_var(class_, i, var);
		const_set_id(class_, i, type);
	}

	return bytes;
}

void parse_class(Exec_Address class_, uint8_t* bytes){
	if(uint32_new(bytes) != 0xC001C0DE){
		fatal_error(PARSER, "Invalid magic number");
	}

	bytes = parse_const_pool(class_, bytes + 4);
	bytes = parse_function_pool(exec_get(class_), bytes);
}
