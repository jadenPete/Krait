#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <jansson.h>

#include <parser/json/classes.h>
#include <loader/archive.h>
#include <loader/classes.h>
#include <storage/data.h>
#include <storage/exec.h>
#include <utils.h>

json_t* root = NULL;

void parse_cj_main_class(void){
	json_t* main_class = json_object_get(root, "mainClass");

	if(!json_is_integer(main_class)){
		fatal_error(PARSER, "classes.json mainClass is not an integer or does not exist");
	}

	*exec_get_main_class() = json_integer_value(main_class);
}

void parse_cj_main_function(void){
	json_t* main_function = json_object_get(root, "mainFunction");

	if(!json_is_integer(main_function)){
		fatal_error(PARSER, "classes.json mainFunction is not an integer or does not exist");
	}

	*exec_get_main_function() = json_integer_value(main_function);
}

void free_classes_json(void){
	json_decref(root);
}

void parse_classes_json(void){
	if(classes.bytes == NULL){
		fatal_error(PARSER, "classes.json does not exist");
	}

	json_error_t error;
	root = json_loadb(classes.bytes, classes.width, 0, &error);

	if(root == NULL){
		fatal_error(PARSER, "classes.json: %s", error.text);
	}

	if(!json_is_object(root)){
		fatal_error(PARSER, "classes.json root is not an object");
	}

	parse_cj_main_class();
	parse_cj_main_function();

	free_classes_json();
}
