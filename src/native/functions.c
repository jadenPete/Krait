#include <stdint.h>
#include <stdio.h>

#include <native/functions.h>
#include <storage/data.h>
#include <types/str.h>

void print_bool(void){
	printf(arg_get(0)->bool_ ? "true" : "false");
}

void print_char(void){
	for(uint8_t i = 0; i < arg_get(0)->char_.width; i++){
		printf("%c", arg_get(0)->char_.bytes[i]);
	}
}

void print_str(void){
	for(Type_Str_Address i = 0; i < arg_get(0)->str_.width; i++){
		for(uint8_t j = 0; j < arg_get(0)->str_.chars[i].width; j++){
			printf("%c", arg_get(0)->str_.chars[i].bytes[j]);
		}
	}
}

void print_int8(void){
	printf("%hhi", (signed char) arg_get(0)->int8_);
}

void print_int16(void){
	printf("%hi", (short int) arg_get(0)->int16_);
}

void print_int32(void){
	printf("%li", (long int) arg_get(0)->int32_);
}

void print_int64(void){
	printf("%lli", (long long int) arg_get(0)->int64_);
}

void print_uint8(void){
	printf("%hhu", (unsigned char) arg_get(0)->uint8_);
}

void print_uint16(void){
	printf("%hu", (unsigned short int) arg_get(0)->uint16_);
}

void print_uint32(void){
	printf("%lu", (unsigned long int) arg_get(0)->uint32_);
}

void print_uint64(void){
	printf("%llu", (unsigned long long int) arg_get(0)->uint64_);
}

void println(void){
	printf("\n");
}
