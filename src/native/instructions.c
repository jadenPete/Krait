#include <stdint.h>

#include <native/instructions.h>
#include <native/functions.h>
#include <storage/data.h>
#include <types/arr.h>
#include <types/bool.h>
#include <types/char.h>
#include <types/str.h>
#include <types/int/int8.h>
#include <types/int/int16.h>
#include <types/int/int32.h>
#include <types/int/int64.h>
#include <types/uint/uint8.h>
#include <types/uint/uint16.h>
#include <types/uint/uint32.h>
#include <types/uint/uint64.h>
#include <runtime.h>
#include <utils.h>

// Instruction functions
const Instruction instruction_ptrs[] = {
	&op_frame_new,         // 0x00
	&op_frame_del,         // 0x01
	&op_const_pull,        // 0x02
	&op_local_alloc,       // 0x03
	&op_local_dealloc,     // 0x04
	&op_local_realloc,     // 0x05
	&op_arg_alloc,         // 0x06
	&op_arg_dealloc,       // 0x07
	&op_arg_realloc,       // 0x08
	&op_arg_push_const,    // 0x09
	&op_arg_push_local,    // 0x0A
	&op_arg_pull,          // 0x0B
	&op_return_push_const, // 0x0C
	&op_return_push_local, // 0x0D
	&op_return_pull,       // 0x0E
	&op_native_call,       // 0x0F
	&op_bool_new,          // 0x10
	&op_bool_new_empty,    // 0x11
	&op_int8_new,          // 0x12
	&op_int8_new_empty,    // 0x13
	&op_int16_new,         // 0x14
	&op_int16_new_empty,   // 0x15
	&op_int32_new,         // 0x16
	&op_int32_new_empty,   // 0x17
	&op_int64_new,         // 0x18
	&op_int64_new_empty,   // 0x19
	&op_uint8_new,         // 0x1A
	&op_uint8_new_empty,   // 0x1B
	&op_uint16_new,        // 0x1C
	&op_uint16_new_empty,  // 0x1D
	&op_uint32_new,        // 0x1E
	&op_uint32_new_empty,  // 0x1F
	&op_uint64_new,        // 0x20
	&op_uint64_new_empty,  // 0x21
	&op_char_dealloc,      // 0x22
	&op_char_replace,      // 0x23
	&op_char_copy,         // 0x24
	&op_char_new_empty,    // 0x25
	&op_str_dealloc,       // 0x26
	&op_str_replace,       // 0x27
	&op_str_replace_at,    // 0x28
	&op_str_resize,        // 0x29
	&op_str_copy,          // 0x2A
	&op_str_new_empty,     // 0x2B
	&op_str_push_const,    // 0x2C
	&op_str_push_local,    // 0x2D
	&op_str_pull,          // 0x2E
	&op_arr_dealloc,       // 0x2F
	&op_arr_replace,       // 0x30
	&op_arr_replace_at,    // 0x31
	&op_arr_resize,        // 0x32
	&op_arr_copy,          // 0x33
	&op_arr_new_empty,     // 0x34
	&op_arr_push_const,    // 0x35
	&op_arr_push_local,    // 0x36
	&op_arr_pull,          // 0x37
	&op_goto,              // 0x38
	&op_goto_if,           // 0x39
	&op_goto_if_not,       // 0x3A
	&op_static_call,       // 0x3B
};

const uint8_t instruction_widths[] = {
	0,  // frame_new
	0,  // frame_del
	4,  // const_pull
	2,  // local_alloc
	0,  // local_dealloc
	2,  // local_realloc
	1,  // arg_alloc
	0,  // arg_dealloc
	1,  // arg_realloc
	3,  // arg_push_const
	4,  // arg_push_local
	3,  // arg_pull
	2,  // return_push_const
	3,  // return_push_local
	2,  // return_pull
	1,  // native_call
	3,  // bool_new
	2,  // bool_new_empty
	3,  // int8_new
	2,  // int8_new_empty
	4,  // int16_new
	2,  // int16_new_empty
	6,  // int32_new
	2,  // int32_new_empty
	10, // int64_new
	2,  // int64_new_empty
	3,  // uint8_new
	2,  // uint8_new_empty
	4,  // uint16_new
	2,  // uint16_new_empty
	6,  // uint32_new
	2,  // uint32_new_empty
	10, // uint64_new
	2,  // uint64_new_empty
	2,  // char_dealloc
	4,  // char_replace
	4,  // char_copy
	2,  // char_new_empty
	2,  // str_dealloc
	4,  // str_replace
	6,  // str_replace_at
	4,  // str_resize
	4,  // str_copy
	2,  // str_new_empty
	6,  // str_push_const
	6,  // str_push_local
	6,  // str_pull
	2,  // arr_dealloc
	4,  // arr_replace
	6,  // arr_replace_at
	4,  // arr_resize
	4,  // arr_copy
	5,  // arr_new_empty
	6,  // arr_push_const
	6,  // arr_push_local
	6,  // arr_pull
	2,  // goto
	4,  // goto_if
	4,  // goto_if_not
	4,  // static_call
};

// Native functions
const Native_Func function_ptrs[] = {
	&type_bool_not,             // 0x00
	&type_bool_and,             // 0x01
	&type_bool_or,              // 0x02
	&type_bool_equal,           // 0x03
	&type_bool_not_equal,       // 0x04
	&type_int8_add,             // 0x05
	&type_int8_subtract,        // 0x06
	&type_int8_plus,            // 0x07
	&type_int8_minus,           // 0x08
	&type_int8_multiply,        // 0x09
	&type_int8_divide,          // 0x0A
	&type_int8_modulo,          // 0x0B
	&type_int8_equal,           // 0x0C
	&type_int8_not_equal,       // 0x0D
	&type_int8_greater,         // 0x0E
	&type_int8_less,            // 0x0F
	&type_int8_greater_equal,   // 0x10
	&type_int8_less_equal,      // 0x11
	&type_int8_bitwise_not,     // 0x12
	&type_int8_bitwise_and,     // 0x13
	&type_int8_bitwise_or,      // 0x14
	&type_int8_bitwise_xor,     // 0x15
	&type_int8_left_shift,      // 0x16
	&type_int8_right_shift,     // 0x17
	&type_int16_add,            // 0x18
	&type_int16_subtract,       // 0x19
	&type_int16_plus,           // 0x1A
	&type_int16_minus,          // 0x1B
	&type_int16_multiply,       // 0x1C
	&type_int16_divide,         // 0x1D
	&type_int16_modulo,         // 0x1E
	&type_int16_equal,          // 0x1F
	&type_int16_not_equal,      // 0x20
	&type_int16_greater,        // 0x21
	&type_int16_less,           // 0x22
	&type_int16_greater_equal,  // 0x23
	&type_int16_less_equal,     // 0x24
	&type_int16_bitwise_not,    // 0x25
	&type_int16_bitwise_and,    // 0x26
	&type_int16_bitwise_or,     // 0x27
	&type_int16_bitwise_xor,    // 0x28
	&type_int16_left_shift,     // 0x29
	&type_int16_right_shift,    // 0x2A
	&type_int32_add,            // 0x2B
	&type_int32_subtract,       // 0x2C
	&type_int32_plus,           // 0x2D
	&type_int32_minus,          // 0x2E
	&type_int32_multiply,       // 0x2F
	&type_int32_divide,         // 0x30
	&type_int32_modulo,         // 0x31
	&type_int32_equal,          // 0x32
	&type_int32_not_equal,      // 0x33
	&type_int32_greater,        // 0x34
	&type_int32_less,           // 0x35
	&type_int32_greater_equal,  // 0x36
	&type_int32_less_equal,     // 0x37
	&type_int32_bitwise_not,    // 0x38
	&type_int32_bitwise_and,    // 0x39
	&type_int32_bitwise_or,     // 0x3A
	&type_int32_bitwise_xor,    // 0x3B
	&type_int32_left_shift,     // 0x3C
	&type_int32_right_shift,    // 0x3D
	&type_int64_add,            // 0x3E
	&type_int64_subtract,       // 0x3F
	&type_int64_plus,           // 0x40
	&type_int64_minus,          // 0x41
	&type_int64_multiply,       // 0x42
	&type_int64_divide,         // 0x43
	&type_int64_modulo,         // 0x44
	&type_int64_equal,          // 0x45
	&type_int64_not_equal,      // 0x46
	&type_int64_greater,        // 0x47
	&type_int64_less,           // 0x48
	&type_int64_greater_equal,  // 0x49
	&type_int64_less_equal,     // 0x4A
	&type_int64_bitwise_not,    // 0x4B
	&type_int64_bitwise_and,    // 0x4C
	&type_int64_bitwise_or,     // 0x4D
	&type_int64_bitwise_xor,    // 0x4E
	&type_int64_left_shift,     // 0x4F
	&type_int64_right_shift,    // 0x50
	&type_uint8_add,            // 0x51
	&type_uint8_subtract,       // 0x52
	&type_uint8_multiply,       // 0x53
	&type_uint8_divide,         // 0x54
	&type_uint8_modulo,         // 0x55
	&type_uint8_equal,          // 0x56
	&type_uint8_not_equal,      // 0x57
	&type_uint8_greater,        // 0x58
	&type_uint8_less,           // 0x59
	&type_uint8_greater_equal,  // 0x5A
	&type_uint8_less_equal,     // 0x5B
	&type_uint8_bitwise_not,    // 0x5C
	&type_uint8_bitwise_and,    // 0x5D
	&type_uint8_bitwise_or,     // 0x5E
	&type_uint8_bitwise_xor,    // 0x5F
	&type_uint8_left_shift,     // 0x60
	&type_uint8_right_shift,    // 0x61
	&type_uint16_add,           // 0x62
	&type_uint16_subtract,      // 0x63
	&type_uint16_multiply,      // 0x64
	&type_uint16_divide,        // 0x65
	&type_uint16_modulo,        // 0x66
	&type_uint16_equal,         // 0x67
	&type_uint16_not_equal,     // 0x68
	&type_uint16_greater,       // 0x69
	&type_uint16_less,          // 0x6A
	&type_uint16_greater_equal, // 0x6B
	&type_uint16_less_equal,    // 0x6C
	&type_uint16_bitwise_not,   // 0x6D
	&type_uint16_bitwise_and,   // 0x6E
	&type_uint16_bitwise_or,    // 0x6F
	&type_uint16_bitwise_xor,   // 0x70
	&type_uint16_left_shift,    // 0x71
	&type_uint16_right_shift,   // 0x72
	&type_uint32_add,           // 0x73
	&type_uint32_subtract,      // 0x74
	&type_uint32_multiply,      // 0x75
	&type_uint32_divide,        // 0x76
	&type_uint32_modulo,        // 0x77
	&type_uint32_equal,         // 0x78
	&type_uint32_not_equal,     // 0x79
	&type_uint32_greater,       // 0x7A
	&type_uint32_less,          // 0x7B
	&type_uint32_greater_equal, // 0x7C
	&type_uint32_less_equal,    // 0x7D
	&type_uint32_bitwise_not,   // 0x7E
	&type_uint32_bitwise_and,   // 0x7F
	&type_uint32_bitwise_or,    // 0x80
	&type_uint32_bitwise_xor,   // 0x81
	&type_uint32_left_shift,    // 0x82
	&type_uint32_right_shift,   // 0x83
	&type_uint64_add,           // 0x84
	&type_uint64_subtract,      // 0x85
	&type_uint64_multiply,      // 0x86
	&type_uint64_divide,        // 0x87
	&type_uint64_modulo,        // 0x88
	&type_uint64_equal,         // 0x89
	&type_uint64_not_equal,     // 0x8A
	&type_uint64_greater,       // 0x8B
	&type_uint64_less,          // 0x8C
	&type_uint64_greater_equal, // 0x8D
	&type_uint64_less_equal,    // 0x8E
	&type_uint64_bitwise_not,   // 0x8F
	&type_uint64_bitwise_and,   // 0x90
	&type_uint64_bitwise_or,    // 0x91
	&type_uint64_bitwise_xor,   // 0x92
	&type_uint64_left_shift,    // 0x93
	&type_uint64_right_shift,   // 0x94
	&type_char_equal,           // 0x95
	&type_char_not_equal,       // 0x96
	&type_str_equal,            // 0x97
	&type_str_not_equal,        // 0x98
	&type_str_width,            // 0x99
	&print_bool,                // 0x9A
	&print_char,                // 0x9B
	&print_str,                 // 0x9C
	&print_int8,                // 0x9D
	&print_int16,               // 0x9E
	&print_int32,               // 0x9F
	&print_int64,               // 0xA0
	&print_uint8,               // 0xA1
	&print_uint16,              // 0xA2
	&print_uint32,              // 0xA3
	&print_uint64,              // 0xA4
	&println,                   // 0xA5
};

const Native_Func_Width function_widths[] = {
	2, // bool_not
	2, // bool_and
	2, // bool_or
	2, // bool_equal
	2, // bool_not_equal
	2, // int8_add
	2, // int8_subtract
	1, // int8_plus
	1, // int8_minus
	2, // int8_multiply
	2, // int8_divide
	2, // int8_modulo
	2, // int8_equal
	2, // int8_not_equal
	2, // int8_greater
	2, // int8_less
	2, // int8_greater_equal
	2, // int8_less_equal
	1, // int8_bitwise_not
	2, // int8_bitwise_and
	2, // int8_bitwise_or
	2, // int8_bitwise_xor
	2, // int8_left_shift
	2, // int8_right_shift
	2, // int16_add
	2, // int16_subtract
	1, // int16_plus
	1, // int16_minus
	2, // int16_multiply
	2, // int16_divide
	2, // int16_modulo
	2, // int16_equal
	2, // int16_not_equal
	2, // int16_greater
	2, // int16_less
	2, // int16_greater_equal
	2, // int16_less_equal
	1, // int16_bitwise_not
	2, // int16_bitwise_and
	2, // int16_bitwise_or
	2, // int16_bitwise_xor
	2, // int16_left_shift
	2, // int16_right_shift
	2, // int32_add
	2, // int32_subtract
	1, // int32_plus
	1, // int32_minus
	2, // int32_multiply
	2, // int32_divide
	2, // int32_modulo
	2, // int32_equal
	2, // int32_not_equal
	2, // int32_greater
	2, // int32_less
	2, // int32_greater_equal
	2, // int32_less_equal
	1, // int32_bitwise_not
	2, // int32_bitwise_and
	2, // int32_bitwise_or
	2, // int32_bitwise_xor
	2, // int32_left_shift
	2, // int32_right_shift
	2, // int64_add
	2, // int64_subtract
	1, // int64_plus
	1, // int64_minus
	2, // int64_multiply
	2, // int64_divide
	2, // int64_modulo
	2, // int64_equal
	2, // int64_not_equal
	2, // int64_greater
	2, // int64_less
	2, // int64_greater_equal
	2, // int64_less_equal
	1, // int64_bitwise_not
	2, // int64_bitwise_and
	2, // int64_bitwise_or
	2, // int64_bitwise_xor
	2, // int64_left_shift
	2, // int64_right_shift
	2, // uint8_add
	2, // uint8_subtract
	2, // uint8_multiply
	2, // uint8_divide
	2, // uint8_modulo
	2, // uint8_equal
	2, // uint8_not_equal
	2, // uint8_greater
	2, // uint8_less
	2, // uint8_greater_equal
	2, // uint8_less_equal
	1, // uint8_bitwise_not
	2, // uint8_bitwise_and
	2, // uint8_bitwise_or
	2, // uint8_bitwise_xor
	2, // uint8_left_shift
	2, // uint8_right_shift
	2, // uint16_add
	2, // uint16_subtract
	2, // uint16_multiply
	2, // uint16_divide
	2, // uint16_modulo
	2, // uint16_equal
	2, // uint16_not_equal
	2, // uint16_greater
	2, // uint16_less
	2, // uint16_greater_equal
	2, // uint16_less_equal
	1, // uint16_bitwise_not
	2, // uint16_bitwise_and
	2, // uint16_bitwise_or
	2, // uint16_bitwise_xor
	2, // uint16_left_shift
	2, // uint16_right_shift
	2, // uint32_add
	2, // uint32_subtract
	2, // uint32_multiply
	2, // uint32_divide
	2, // uint32_modulo
	2, // uint32_equal
	2, // uint32_not_equal
	2, // uint32_greater
	2, // uint32_less
	2, // uint32_greater_equal
	2, // uint32_less_equal
	1, // uint32_bitwise_not
	2, // uint32_bitwise_and
	2, // uint32_bitwise_or
	2, // uint32_bitwise_xor
	2, // uint32_left_shift
	2, // uint32_right_shift
	2, // uint64_add
	2, // uint64_subtract
	2, // uint64_multiply
	2, // uint64_divide
	2, // uint64_modulo
	2, // uint64_equal
	2, // uint64_not_equal
	2, // uint64_greater
	2, // uint64_less
	2, // uint64_greater_equal
	2, // uint64_less_equal
	1, // uint64_bitwise_not
	2, // uint64_bitwise_and
	2, // uint64_bitwise_or
	2, // uint64_bitwise_xor
	2, // uint64_left_shift
	2, // uint64_right_shift
	2, // char_equal
	2, // char_not_equal
	2, // str_equal
	2, // str_not_equal
	1, // str_width
	1, // print_bool
	1, // print_char
	1, // print_str
	1, // print_int8
	1, // print_int16
	1, // print_int32
	1, // print_int64
	1, // print_uint8
	1, // print_uint16
	1, // print_uint32
	1, // print_uint64
	0, // println
};

void instruction_exec(uint8_t* bytes){
	instruction_ptrs[bytes[0]](bytes + 1);
}

uint8_t instruction_width(uint8_t byte){
	return instruction_widths[byte];
}

void op_frame_new(uint8_t* bytes){
	frames_increment();
}

void op_frame_del(uint8_t* bytes){
	frames_decrement();
}

void op_local_alloc(uint8_t* bytes){
	local_alloc(uint16_new(bytes));
}

void op_local_dealloc(uint8_t* bytes){
	local_dealloc();
}

void op_local_realloc(uint8_t* bytes){
	local_realloc(uint16_new(bytes));
}

void op_arg_alloc(uint8_t* bytes){
	arg_alloc(bytes[0]);
}

void op_arg_dealloc(uint8_t* bytes){
	arg_dealloc();
}

void op_arg_realloc(uint8_t* bytes){
	arg_realloc(bytes[0]);
}

void op_arg_push_const(uint8_t* bytes){
	*arg_get(bytes[0]) = var_copy(const_get_var(uint16_new(bytes + 1)),
	                              const_get_id(uint16_new(bytes + 1)));
}

void op_arg_push_local(uint8_t* bytes){
	*arg_get(bytes[0]) = var_copy(*local_get(uint16_new(bytes + 1)), bytes[3]);
}

void op_arg_pull(uint8_t* bytes){
	*local_get(uint16_new(bytes)) = *arg_get(bytes[2]);
}

void op_return_push_const(uint8_t* bytes){
	*return_get() = var_copy(const_get_var(uint16_new(bytes)),
	                         const_get_id(uint16_new(bytes)));
}

void op_return_push_local(uint8_t* bytes){
	*return_get() = var_copy(*local_get(uint16_new(bytes)), bytes[2]);
}

void op_return_pull(uint8_t* bytes){
	*local_get(uint16_new(bytes)) = *return_get();
}

void op_const_pull(uint8_t* bytes){
	*local_get(uint16_new(bytes)) = var_copy(const_get_var(uint16_new(bytes + 2)),
	                                         const_get_id(uint16_new(bytes + 2)));
}

void op_int8_new(uint8_t* bytes){
	local_get(uint16_new(bytes))->int8_ = bytes[2];
}

void op_int8_new_empty(uint8_t* bytes){
	local_get(uint16_new(bytes))->int8_ = 0;
}

void op_int16_new(uint8_t* bytes){
	local_get(uint16_new(bytes))->int16_ = int16_new(bytes + 2);
}

void op_int16_new_empty(uint8_t* bytes){
	local_get(uint16_new(bytes))->int16_ = 0;
}

void op_int32_new(uint8_t* bytes){
	local_get(uint16_new(bytes))->int32_ = int32_new(bytes + 2);
}

void op_int32_new_empty(uint8_t* bytes){
	local_get(uint16_new(bytes))->int32_ = 0;
}

void op_int64_new(uint8_t* bytes){
	local_get(uint16_new(bytes))->int64_ = int64_new(bytes + 2);
}

void op_int64_new_empty(uint8_t* bytes){
	local_get(uint16_new(bytes))->int64_ = 0;
}

void op_uint8_new(uint8_t* bytes){
	local_get(uint16_new(bytes))->uint8_ = bytes[2];
}

void op_uint8_new_empty(uint8_t* bytes){
	local_get(uint16_new(bytes))->uint8_ = 0;
}

void op_uint16_new(uint8_t* bytes){
	local_get(uint16_new(bytes))->uint16_ = uint16_new(bytes + 2);
}

void op_uint16_new_empty(uint8_t* bytes){
	local_get(uint16_new(bytes))->uint16_ = 0;
}

void op_uint32_new(uint8_t* bytes){
	local_get(uint16_new(bytes))->uint32_ = uint32_new(bytes + 2);
}

void op_uint32_new_empty(uint8_t* bytes){
	local_get(uint16_new(bytes))->uint32_ = 0;
}

void op_uint64_new(uint8_t* bytes){
	local_get(uint16_new(bytes))->uint64_ = uint64_new(bytes + 2);
}

void op_uint64_new_empty(uint8_t* bytes){
	local_get(uint16_new(bytes))->uint64_ = 0;
}

void op_arr_dealloc(uint8_t* bytes){
	type_arr_dealloc(local_get(uint16_new(bytes))->arr_);
}

void op_arr_replace(uint8_t* bytes){
	type_arr_replace(&local_get(uint16_new(bytes))->arr_, local_get(uint16_new(bytes + 2))->arr_);
}

void op_arr_replace_at(uint8_t* bytes){
	type_arr_replace_at(&local_get(uint16_new(bytes))->arr_,
	                     local_get(uint16_new(bytes + 2))->arr_,
	                     local_get(uint16_new(bytes + 4))->uint32_);
}

void op_arr_resize(uint8_t* bytes){
	type_arr_resize(&local_get(uint16_new(bytes))->arr_, local_get(uint16_new(bytes + 2))->uint32_);
}

void op_arr_copy(uint8_t* bytes){
	local_get(uint16_new(bytes))->arr_ = type_arr_copy(local_get(uint16_new(bytes + 2))->arr_);
}

void op_arr_new_empty(uint8_t* bytes){
	local_get(uint16_new(bytes))->arr_ = type_arr_new_empty(bytes[2], local_get(uint16_new(bytes + 4))->uint32_);
}

void op_arr_push_const(uint8_t* bytes){
	Type_Arr arr_ = local_get(uint16_new(bytes))->arr_;
	Type_Arr_Address addr = local_get(uint16_new(bytes + 2))->uint32_;

	*type_arr_get(arr_, addr) = var_copy(const_get_var(uint16_new(bytes + 4)), arr_.width);
}

void op_arr_push_local(uint8_t* bytes){
	Type_Arr arr_ = local_get(uint16_new(bytes))->arr_;
	Type_Arr_Address addr = local_get(uint16_new(bytes + 2))->uint32_;

	*type_arr_get(arr_, addr) = var_copy(*local_get(uint16_new(bytes + 4)), arr_.width);
}

void op_arr_pull(uint8_t* bytes){
	*local_get(uint16_new(bytes)) = *type_arr_get(local_get(uint16_new(bytes + 2))->arr_,
	                                              local_get(uint16_new(bytes + 4))->uint32_);
}

void op_bool_new(uint8_t* bytes){
	local_get(uint16_new(bytes))->bool_ = bytes[2];
}

void op_bool_new_empty(uint8_t* bytes){
	local_get(uint16_new(bytes))->bool_ = 0;
}

void op_char_dealloc(uint8_t* bytes){
	type_char_dealloc(local_get(uint16_new(bytes))->char_);
}

void op_char_replace(uint8_t* bytes){
	type_char_replace(&local_get(uint16_new(bytes))->char_, local_get(uint16_new(bytes + 2))->char_);
}

void op_char_copy(uint8_t* bytes){
	local_get(uint16_new(bytes))->char_ = type_char_copy(local_get(uint16_new(bytes + 2))->char_);
}

void op_char_new_empty(uint8_t* bytes){
	local_get(uint16_new(bytes))->char_ = type_char_new_empty();
}

void op_str_dealloc(uint8_t* bytes){
	type_str_dealloc(local_get(uint16_new(bytes))->str_);
}

void op_str_replace(uint8_t* bytes){
	type_str_replace(&local_get(uint16_new(bytes))->str_, local_get(uint16_new(bytes + 2))->str_);
}

void op_str_replace_at(uint8_t* bytes){
	type_str_replace_at(&local_get(uint16_new(bytes))->str_,
	                     local_get(uint16_new(bytes + 2))->str_,
	                     local_get(uint16_new(bytes + 4))->uint32_);
}

void op_str_resize(uint8_t* bytes){
	type_str_resize(&local_get(uint16_new(bytes))->str_, local_get(uint16_new(bytes + 2))->uint32_);
}

void op_str_copy(uint8_t* bytes){
	local_get(uint16_new(bytes))->str_ = type_str_copy(local_get(uint16_new(bytes + 2))->str_);
}

void op_str_new_empty(uint8_t* bytes){
	local_get(uint16_new(bytes))->str_ = type_str_new_empty();
}

void op_str_push_const(uint8_t* bytes){
	*type_str_get(local_get(uint16_new(bytes))->str_,local_get(uint16_new(bytes + 2))->uint32_) =
	type_char_copy(const_get_var(uint16_new(bytes + 4)).char_);
}

void op_str_push_local(uint8_t* bytes){
	*type_str_get(local_get(uint16_new(bytes))->str_, local_get(uint16_new(bytes + 2))->uint32_) =
	type_char_copy(local_get(uint16_new(bytes + 4))->char_);
}

void op_str_pull(uint8_t* bytes){
	local_get(uint16_new(bytes))->char_ = *type_str_get(local_get(uint16_new(bytes + 2))->str_,
	                                                    local_get(uint16_new(bytes + 4))->uint32_);
}

void op_native_call(uint8_t* bytes){
	frames_next();
	function_ptrs[bytes[0]]();
	frames_previous();
}

void op_static_call(uint8_t* bytes){
	frames_next();
	runtime_call(uint16_new(bytes), uint16_new(bytes + 2));
	frames_previous();
}

void op_goto(uint8_t* bytes){
	runtime_redirect(uint16_new(bytes));
}

void op_goto_if(uint8_t* bytes){
	if(local_get(uint16_new(bytes + 2))->bool_){
		runtime_redirect(uint16_new(bytes));
	}
}

void op_goto_if_not(uint8_t* bytes){
	if(!local_get(uint16_new(bytes + 2))->bool_){
		runtime_redirect(uint16_new(bytes));
	}
}
