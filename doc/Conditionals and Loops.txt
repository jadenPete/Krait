General Conditionals and Loops

If
1	goto_if_not 3
2	...
3	...

If-Else
1	goto_if 4
2		Else
3	goto 5
4		If
5	...

While
1	goto 3
2		While
3	goto_if 2
4	...

Do-While
1		While
2	goto_if 1
3	...
